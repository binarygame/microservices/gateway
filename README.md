# BinaryGame Gateway

This project holds the code for BinaryGame's API Gateway

The purpose of this project is to act as the intermediary between the microservices and the external world, simplifying the external API interface for frontend developers

## Supported Protocols

Internally, this project uses the default golang HTTP library alongside ConnectRPC.

Because of ConnectRPC, the server supports plain HTTP, gRPC and Connect protocols.

## Credits

- Project icon designed by `Iconjam` from [Flaticon](https://www.flaticon.com/free-icon/router_5537337).
