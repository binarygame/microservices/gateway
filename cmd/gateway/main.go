package main

import (
	"context"
	"log/slog"

	"github.com/joho/godotenv"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/constants"
	"gitlab.com/binarygame/microservices/gateway/internal/server/connectrpc/service"
)

func main() {
	err := godotenv.Load()
	logger := bingameutils.GetLogger()
	slog.SetDefault(logger)

	if err != nil {
		logger.Info("Error loading .env file", "error", err.Error())
	}

	// Set up OpenTelemetry.
	ts, otelShutdown, err := telemetry.New(context.Background(), constants.NamespaceKey)
	if err != nil {
		slog.Error("failed to set up OpenTelemetry SDK", "error", err)
	}
	defer func() {
		slog.Error("Error in otelShutdown", "error", otelShutdown(context.Background()))
	}()

	host := bingameutils.GetServiceHost()
	port := bingameutils.GetServicePort()

	service.Start(host, port, logger, ts)
}
