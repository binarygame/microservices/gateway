module gitlab.com/binarygame/microservices/gateway

go 1.22.7

toolchain go1.23.4

// replace gitlab.com/binarygame/microservices/bingame-utils => /home/zerowhy/repos/binarygame/microservices/bingame-utils

require (
	connectrpc.com/connect v1.17.0
	connectrpc.com/cors v0.1.0
	connectrpc.com/grpcreflect v1.2.0
	connectrpc.com/otelconnect v0.7.1
	github.com/google/uuid v1.6.0
	github.com/joho/godotenv v1.5.1
	github.com/oklog/ulid/v2 v2.1.0
	github.com/rs/cors v1.11.1
	gitlab.com/binarygame/microservices/bingame-utils v0.6.10
	gitlab.com/binarygame/microservices/guesses v0.6.1
	gitlab.com/binarygame/microservices/questions v0.7.0
	gitlab.com/binarygame/microservices/rooms v0.13.0
	gitlab.com/binarygame/microservices/users v0.6.3
	golang.org/x/net v0.33.0
	google.golang.org/protobuf v1.36.1
)

require (
	github.com/cenkalti/backoff/v4 v4.3.0 // indirect
	github.com/gabriel-vasile/mimetype v1.4.7 // indirect
	github.com/go-logr/logr v1.4.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/go-playground/validator/v10 v10.23.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.25.1 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	go.opentelemetry.io/auto/sdk v1.1.0 // indirect
	go.opentelemetry.io/otel v1.33.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetricgrpc v1.33.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlptrace v1.33.0 // indirect
	go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc v1.33.0 // indirect
	go.opentelemetry.io/otel/metric v1.33.0 // indirect
	go.opentelemetry.io/otel/sdk v1.33.0 // indirect
	go.opentelemetry.io/otel/sdk/metric v1.33.0 // indirect
	go.opentelemetry.io/otel/trace v1.33.0 // indirect
	go.opentelemetry.io/proto/otlp v1.4.0 // indirect
	golang.org/x/crypto v0.31.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/text v0.21.0 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20241223144023-3abc09e42ca8 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241223144023-3abc09e42ca8 // indirect
	google.golang.org/grpc v1.69.2 // indirect
)
