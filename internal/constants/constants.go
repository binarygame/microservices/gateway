package constants

import "errors"

// Generic errors
var ErrUserAuthFailed = errors.New("user not authenticated or authentication invalid")

// Guesses errors
var ErrMatchNotPlaying = errors.New("can't submit a guess to a room that is not currently playing")

const NamespaceKey = "gitlab.com/binarygame/microservices/gateway"
