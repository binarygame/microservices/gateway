package handlers

import (
	"context"
	"errors"
	"log/slog"
	"sync"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers/events"
	guessesModel "gitlab.com/binarygame/microservices/guesses/pkg/models"
	roomsModel "gitlab.com/binarygame/microservices/rooms/pkg/model"
)

type eventsHandler struct {
	users       UsersHandler
	rooms       RoomsHandler
	questions   QuestionsHandler
	guesses     GuessesHandler
	logger      *slog.Logger
	roomManager *bingameutils.RoomManager[events.Event]
	telemetry   telemetry.TelemetryService
}

type EventsHandler interface {
	SubscribeToRoom(ctx context.Context, roomId string) (<-chan events.Event, error)
}

type RoomSubscription struct {
	clients map[chan events.Event]struct{}
	roomId  string
	mu      sync.RWMutex
}

func NewEventsHandler(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) EventsHandler {
	return &eventsHandler{
		users:       NewUsersHandler(logger, ts, clientInterceptor),
		rooms:       NewRoomsHandler(logger, ts, clientInterceptor),
		questions:   NewQuestionsHandler(logger, ts, clientInterceptor),
		guesses:     NewGuessesHandler(logger, ts, clientInterceptor),
		logger:      logger,
		roomManager: bingameutils.NewRoomManager[events.Event](),
		telemetry:   ts,
	}
}

// SubscribeToRoom subscribes a client to the events of a specific room
func (e *eventsHandler) SubscribeToRoom(ctx context.Context, roomId string) (<-chan events.Event, error) {
	span := e.telemetry.Record(ctx, "SubscribeToRoom", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, errors.New("user authentication failed")
	}

	client := bingameutils.NewRoomManagerClient[events.Event]()
	e.roomManager.SubscribeClient(client, roomId)

	go func() {
		<-ctx.Done()
		e.roomManager.UnsubscribeClient(client.ClientID, roomId)
		close(client.Channel)
	}()

	// Start event handlers if it's a new room subscription
	if len(e.roomManager.GetOrCreateRoom(roomId).Clients) == 1 {
		go e.startReceivingRoomEvents(context.Background(), roomId)
		go e.startReceivingGuessesEvents(context.Background(), roomId)
	}

	return client.Channel, nil
}

func (e *eventsHandler) startReceivingGuessesEvents(ctx context.Context, roomId string) {
	span := e.telemetry.Record(ctx, "startReceivingGuessesEvents", map[string]string{"roomId": roomId})
	defer span.End()

	eventStream, err := e.guesses.SubscribeToRoom(ctx, roomId)
	if err != nil {
		e.logger.Error("Error subscribing to guesses events", "error", err.Error(), "roomId", roomId)
		return
	}

	for {
		select {
		case <-ctx.Done():
			return
		case event, ok := <-eventStream:
			if !ok {
				return
			}
			newEvent, err := e.translateGuessesEvent(event)
			if err != nil {
				e.logger.Error("Failed to convert guesses event into gateway event", "error", err.Error())
				continue
			}
			e.roomManager.Broadcast(newEvent, roomId)
		}
	}
}

var guessesEventTypeToProto = map[guessesModel.EventType]events.EventType{
	guessesModel.GuessSubmitted:     events.GuessSubmitted,
	guessesModel.LeaderboardUpdated: events.LeaderboardUpdated,
}

func (e *eventsHandler) translateGuessesEvent(guessesEvent guessesModel.Event) (events.Event, error) {
	span := e.telemetry.Record(context.Background(), "translateGuessesEvent", map[string]string{
		"eventId":     guessesEvent.GetID(),
		"description": guessesEvent.GetDescription(),
		"eventType":   guessesEvent.GetType().String(),
		"roomId":      guessesEvent.GetRoomID(),
		"timestamp":   guessesEvent.GetTimestamp().String(),
	})
	defer span.End()

	logger := e.logger.With(slog.Group("input", slog.Group("guesses_event",
		slog.String("id", guessesEvent.GetID()),
		slog.String("description", guessesEvent.GetDescription()),
		slog.Any("type", guessesEvent.GetType()),
		slog.String("room_id", guessesEvent.GetRoomID()),
		slog.Time("timestamp", guessesEvent.GetTimestamp()),
	)))

	logger.Debug("Converting guesses event to gateway event")

	eventType, ok := guessesEventTypeToProto[guessesEvent.GetType()]
	if !ok {
		logger.Error("Received invalid guesses event type")
		return nil, ErrRoomEventTypeInvalid
	}

	// TODO: Remove this by fixing guessesModel
	description := guessesEvent.GetDescription()
	baseEvent := events.BaseEvent{
		ID:          guessesEvent.GetID(),
		Timestamp:   guessesEvent.GetTimestamp(),
		Type:        eventType,
		Description: &description,
		RoomID:      guessesEvent.GetRoomID(),
	}

	var parsedEvent events.Event

	switch event := guessesEvent.(type) {
	case *guessesModel.GuessEvent:
		logger.Debug("Event detected as GuessEvent")

		parsedEvent = &events.GuessEvent{
			BaseEvent:  baseEvent,
			UserID:     event.UserId,
			QuestionID: event.QuestionId,
			Correct:    event.Correct,
		}
	case *guessesModel.LeaderboardUpdatedEvent:
		logger.Debug("Event detected as LeaderboardUpdatedEvent")

		parsedEvent = &events.LeaderboardUpdatedEvent{
			BaseEvent:   baseEvent,
			Leaderboard: *event.Leaderboard,
		}
	default:
		logger.Debug("Guesses Event type has no special handling")
		parsedEvent = &baseEvent
	}

	if parsedEvent == nil {
		logger.Error("After parsing event parsedEvent variable is nil")
		return nil, ErrParsedEventNil
	}
	logger.Debug("Finalized parsing of room event with no errors")
	return parsedEvent, nil
}

// startReceivingRoomEvents receives events from rooms microservice and broadcasts them to subscribed clients
func (e *eventsHandler) startReceivingRoomEvents(ctx context.Context, roomId string) {
	span := e.telemetry.Record(ctx, "startReceivingRoomEvents", map[string]string{"roomId": roomId})
	defer span.End()

	eventStream, err := e.rooms.SubscribeToRoom(ctx, roomId)
	if err != nil {
		e.logger.Error("Error subscribing to room events", "error", err.Error(), "roomId", roomId)
		return
	}

	for {
		select {
		case <-ctx.Done():
			return
		case event, ok := <-eventStream:
			if !ok {
				return
			}
			newEvent, err := e.translateRoomsEvent(ctx, event)
			if err != nil {
				e.logger.Error("Failed to convert room event into gateway event", "error", err.Error())
				continue
			}
			e.roomManager.Broadcast(newEvent, roomId)
		}
	}
}

// translateEvent translates the specific room event to a generic event
// TODO: think about adding this to the rooms service, this is insanity

var roomEventTypeToProto = map[roomsModel.EventType]events.EventType{
	roomsModel.RoomEnding:         events.RoomEnding,
	roomsModel.RoomPlaying:        events.RoomPlaying,
	roomsModel.RoomStarting:       events.RoomStarting,
	roomsModel.UserHostChanged:    events.UserHostChanged,
	roomsModel.UserJoined:         events.UserJoined,
	roomsModel.UserLeft:           events.UserLeft,
	roomsModel.UserLostConnection: events.UserLostConnection,
	roomsModel.UserBanned:         events.UserBanned,
}

var (
	ErrRoomEventTypeInvalid = errors.New("rooms event type does not match with any gateway event type")
	ErrParsedEventNil       = errors.New("after parsing event is nil")
)

func (e *eventsHandler) translateRoomsEvent(ctx context.Context, roomsEvent roomsModel.Event) (events.Event, error) {
	span := e.telemetry.Record(ctx, "translateRoomsEvent", map[string]string{
		"eventId":     roomsEvent.GetID(),
		"description": roomsEvent.GetDescription(),
		"eventType":   roomsEvent.GetType().String(),
		"roomId":      roomsEvent.GetRoomID(),
		"timestamp":   roomsEvent.GetTimestamp().String(),
	})
	defer span.End()

	logger := e.logger.With(slog.Group("input", slog.Group("rooms_event",
		slog.String("id", roomsEvent.GetID()),
		slog.String("description", roomsEvent.GetDescription()),
		slog.Any("type", roomsEvent.GetType()),
		slog.String("room_id", roomsEvent.GetRoomID()),
		slog.Time("timestamp", roomsEvent.GetTimestamp()),
	)))

	logger.Debug("Converting room event to gateway event")

	eventType, ok := roomEventTypeToProto[roomsEvent.GetType()]
	if !ok {
		logger.Error("Received invalid rooms event type")
		return nil, ErrRoomEventTypeInvalid
	}

	// TODO: Remove this by fixing roomsModel
	description := roomsEvent.GetDescription()
	baseEvent := events.BaseEvent{
		ID:          roomsEvent.GetID(),
		Timestamp:   roomsEvent.GetTimestamp(),
		Type:        eventType,
		Description: &description,
		RoomID:      roomsEvent.GetRoomID(),
	}

	var parsedEvent events.Event

	switch event := roomsEvent.(type) {
	case *roomsModel.UserEvent:
		if event.GetType() == roomsModel.UserJoined {
			logger.Debug("Event detected as UserJoinedEvent")

			// Get user data
			u, err := e.users.GetUser(ctx, event.UserId)
			if err != nil {
				logger.Error("Could not get user data from user id", "error", err)
				return nil, err
			}
			parsedEvent = &events.UserJoinedEvent{
				UserEvent: events.UserEvent{
					BaseEvent: baseEvent,
					UserId:    event.UserId,
				},
				Emoji:    u.Emoji,
				Nickname: u.Nickname,
			}
		} else {
			logger.Debug("Event detected as UserEvent")

			parsedEvent = &events.UserEvent{
				BaseEvent: baseEvent,
				UserId:    event.UserId,
			}
		}
	case *roomsModel.RoomStartingEvent:
		logger.Debug("Event detected as RoomStartingEvent")
		questionSet, err := e.questions.GetQuestionSet(ctx, event.GetQuestionSetId())
		if err != nil {
			logger.Error("Could not get question set from id", "error", err.Error())
			return nil, err
		}

		if logger.Enabled(ctx, slog.LevelDebug) {
			questionsLog := make([]slog.Attr, len(questionSet.Questions))
			for i, q := range questionSet.Questions {
				questionsLog[i] = slog.Group(q.GetID(),
					slog.String("id", q.GetID()),
					slog.Any("type", q.GetType().String()),
					slog.String("query", q.GetQuery()),
					slog.String("answer", q.GetAnswer()),
					slog.Float64("difficulty", float64(q.GetDifficulty())),
				)
			}
			logger.Debug("Got question set from questions microservice", slog.Group("question_set",
				"id", questionSet.ID,
				"level", questionSet.Level,
				"questions", questionsLog,
			))
		}

		parsedEvent = &events.RoomStartingEvent{
			BaseEvent:   baseEvent,
			StartsIn:    event.StartsIn,
			QuestionSet: *questionSet,
		}
	default:
		parsedEvent = &baseEvent
	}

	if parsedEvent == nil {
		logger.Error("After parsing event parsedEvent variable is nil")
		return nil, ErrParsedEventNil
	}
	logger.Debug("Finalized parsing of room event with no errors")
	return parsedEvent, nil
}
