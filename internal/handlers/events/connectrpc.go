package events

/*
  Note: All of the code in this file seems like it is completely unnecessary.
  I don't know any other way to do it though, relying on the numbers in the
  enums matching seems like a recipe for disaster.
*/

import (
	"errors"
	"log/slog"

	eventsv1 "gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/events/v1"
	guessesConstants "gitlab.com/binarygame/microservices/guesses/pkg/constants"
	questionsModels "gitlab.com/binarygame/microservices/questions/pkg/models/questions"

	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	ErrReceivedNilEvent         = errors.New("received nil event")
	ErrNoHandlerForEvent        = errors.New("no handler was found for the event type")
	ErrEventTypeHandlerMismatch = errors.New("handler received event type it doesn't handle")
	ErrQuestionSetLevelInvalid  = errors.New("received invalid question set level")
	ErrQuestionTypeInvalid      = errors.New("received invalid question type")
)

var questionSetLevelToProtoMap = map[questionsModels.QuestionSetLevel]eventsv1.DifficultyLevel{
	questionsModels.Unknown: eventsv1.DifficultyLevel_DIFFICULTY_LEVEL_UNSPECIFIED,
	questionsModels.Easy:    eventsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY,
	questionsModels.Medium:  eventsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM,
	questionsModels.Hard:    eventsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD,
}

var questionTypeToProtoMap = map[questionsModels.QuestionType]eventsv1.QuestionType{
	questionsModels.BinaryToDecimal: eventsv1.QuestionType_QUESTION_TYPE_BINARY_TO_DECIMAL,
	questionsModels.DecimalToBinary: eventsv1.QuestionType_QUESTION_TYPE_DECIMAL_TO_BINARY,
}

var eventTypeToProtoMap = map[EventType]eventsv1.EventType{
	RoomStarting:       eventsv1.EventType_EVENT_TYPE_ROOM_STARTING,
	RoomPlaying:        eventsv1.EventType_EVENT_TYPE_ROOM_PLAYING,
	RoomEnding:         eventsv1.EventType_EVENT_TYPE_ROOM_ENDING,
	UserHostChanged:    eventsv1.EventType_EVENT_TYPE_ROOM_USER_HOST_CHANGED,
	UserJoined:         eventsv1.EventType_EVENT_TYPE_ROOM_USER_JOINED,
	UserLeft:           eventsv1.EventType_EVENT_TYPE_ROOM_USER_LEFT,
	UserLostConnection: eventsv1.EventType_EVENT_TYPE_ROOM_USER_LOST_CONNECTION,
	UserBanned:         eventsv1.EventType_EVENT_TYPE_ROOM_USER_BANNED,
	UserUnbanned:       eventsv1.EventType_EVENT_TYPE_ROOM_USER_UNBANNED,

	GuessSubmitted:     eventsv1.EventType_EVENT_TYPE_GUESS_SUBMITTED,
	LeaderboardUpdated: eventsv1.EventType_EVENT_TYPE_LEADERBOARD_UPDATED,
}

type protoEventCreator func(Event) (*eventsv1.Event, error)

var eventTypeHandlerMap = map[EventType]protoEventCreator{
	RoomStarting:       roomStartingHandler,
	RoomPlaying:        genericHandler(RoomPlaying),
	RoomEnding:         genericHandler(RoomEnding),
	UserHostChanged:    userHandler(UserHostChanged),
	UserJoined:         userJoinedHandler,
	UserLeft:           userHandler(UserLeft),
	UserLostConnection: userHandler(UserLostConnection),
	UserBanned:         userHandler(UserBanned),
	UserUnbanned:       userHandler(UserUnbanned),

	GuessSubmitted:     guessHandler,
	LeaderboardUpdated: leaderboardUpdatedHandler,
}

func genericHandler(eventType EventType) protoEventCreator {
	t, ok := eventTypeToProtoMap[eventType]
	if !ok {
		return func(Event) (*eventsv1.Event, error) { return nil, ErrEventTypeHandlerMismatch }
	}

	return func(e Event) (*eventsv1.Event, error) {
		return &eventsv1.Event{
			EventId:     e.GetID(),
			EventTime:   timestamppb.New(e.GetTimestamp()),
			RoomId:      e.GetRoomID(),
			Description: e.GetDescription(),
			EventType:   t,
			Event: &eventsv1.Event_GenericEvent{
				GenericEvent: &eventsv1.GenericEvent{},
			},
		}, nil
	}
}

func userHandler(eventType EventType) protoEventCreator {
	t, ok := eventTypeToProtoMap[eventType]
	if !ok {
		return func(Event) (*eventsv1.Event, error) { return nil, ErrEventTypeHandlerMismatch }
	}

	return func(e Event) (*eventsv1.Event, error) {
		event := e.(*UserEvent)
		if !ok {
			return nil, ErrEventTypeHandlerMismatch
		}
		return &eventsv1.Event{
			EventId:     e.GetID(),
			EventTime:   timestamppb.New(e.GetTimestamp()),
			RoomId:      e.GetRoomID(),
			Description: e.GetDescription(),
			EventType:   t,
			Event: &eventsv1.Event_UserEvent{
				UserEvent: &eventsv1.RoomUserEvent{
					UserId: event.UserId,
				},
			},
		}, nil
	}
}

func userJoinedHandler(e Event) (*eventsv1.Event, error) {
	event, ok := e.(*UserJoinedEvent)
	if !ok {
		slog.Debug("Using userJoinedHandler but event type does not match", "event_type", e.GetType())
		return nil, ErrEventTypeHandlerMismatch
	}

	return &eventsv1.Event{
		EventId:     event.GetID(),
		EventTime:   timestamppb.New(event.GetTimestamp()),
		EventType:   eventsv1.EventType_EVENT_TYPE_ROOM_USER_JOINED,
		RoomId:      event.GetRoomID(),
		Description: event.GetDescription(),
		Event: &eventsv1.Event_UserJoinedEvent{
			UserJoinedEvent: &eventsv1.UserJoinedEvent{
				UserId:   event.UserId,
				Nickname: event.Nickname,
				Emoji:    event.Emoji,
			},
		},
	}, nil
}

func roomStartingHandler(e Event) (*eventsv1.Event, error) {
	event, ok := e.(*RoomStartingEvent)
	if !ok {
		slog.Debug("Using roomStartingHandler but event type does not match", "event_type", e.GetType())
		return nil, ErrEventTypeHandlerMismatch
	}

	qSet := event.GetQuestionSet()
	level, ok := questionSetLevelToProtoMap[qSet.Level]
	if !ok {
		slog.Debug("Could not convert question set level to protobuf equivalent", "question_level", qSet.Level)
		return nil, ErrQuestionSetLevelInvalid
	}

	questions := make([]*eventsv1.Question, len(qSet.Questions))
	for i, q := range qSet.Questions {
		qType, ok := questionTypeToProtoMap[q.GetType()]
		if !ok {
			slog.Debug("Could not convert question type to protobuf equivalent", "question_type", q.GetType())
			return nil, ErrQuestionTypeInvalid
		}

		questions[i] = &eventsv1.Question{
			Id:     q.GetID(),
			Type:   qType,
			Query:  q.GetQuery(),
			Answer: q.GetAnswer(),
		}
	}

	return &eventsv1.Event{
		EventId:     event.GetID(),
		EventTime:   timestamppb.New(event.GetTimestamp()),
		EventType:   eventsv1.EventType_EVENT_TYPE_ROOM_STARTING,
		RoomId:      event.GetRoomID(),
		Description: event.GetDescription(),
		Event: &eventsv1.Event_RoomStartingEvent{
			RoomStartingEvent: &eventsv1.RoomStartingEvent{
				StartsIn: durationpb.New(event.StartsIn),
				Questions: &eventsv1.QuestionSet{
					Id:        qSet.ID,
					Level:     level,
					Questions: questions,
				},
			},
		},
	}, nil
}

func guessHandler(e Event) (*eventsv1.Event, error) {
	event, ok := e.(*GuessEvent)
	if !ok {
		slog.Debug("Using guessHandler but event type does not match", "event_type", e.GetType())
		return nil, ErrEventTypeHandlerMismatch
	}

	return &eventsv1.Event{
		EventId:     event.GetID(),
		EventTime:   timestamppb.New(event.GetTimestamp()),
		EventType:   eventsv1.EventType_EVENT_TYPE_GUESS_SUBMITTED,
		RoomId:      event.GetRoomID(),
		Description: event.GetDescription(),
		Event: &eventsv1.Event_GuessEvent{
			GuessEvent: &eventsv1.GuessEvent{
				UserId:     event.UserID,
				QuestionId: event.QuestionID,
				Correct:    event.Correct,
			},
		},
	}, nil
}

func leaderboardUpdatedHandler(e Event) (*eventsv1.Event, error) {
	event, ok := e.(*LeaderboardUpdatedEvent)
	if !ok {
		slog.Debug("Using leaderboardUpdatedHandler but event type does not match", "event_type", e.GetType())
		return nil, ErrEventTypeHandlerMismatch
	}

	// Convert rankElements to protobuf format
	originalRankList := event.GetLeaderboard().RankList
	ranks := make([]*eventsv1.RankElement, len(originalRankList))
	for i, r := range originalRankList {
		ranks[i] = &eventsv1.RankElement{
			UserId:             r.UserID,
			Score:              float32(r.Score),
			ComboMultiplierIdx: r.ComboMultiplierIdx,
			ComboMultiplier:    float32(guessesConstants.MultiplierProgression[r.ComboMultiplierIdx]),
		}
	}

	return &eventsv1.Event{
		EventId:     event.GetID(),
		EventTime:   timestamppb.New(event.GetTimestamp()),
		EventType:   eventsv1.EventType_EVENT_TYPE_LEADERBOARD_UPDATED,
		RoomId:      event.GetRoomID(),
		Description: event.GetDescription(),
		Event: &eventsv1.Event_LeaderboardUpdatedEvent{
			LeaderboardUpdatedEvent: &eventsv1.LeaderboardUpdatedEvent{
				Data: &eventsv1.LeaderboardData{
					Ranks: ranks,
				},
			},
		},
	}, nil
}

func TranslateEventToProto(event Event) (*eventsv1.Event, error) {
	if event == nil {
		slog.Debug("Received nil event in TranslateEventToProto")
		return nil, ErrReceivedNilEvent
	}

	handler, ok := eventTypeHandlerMap[event.GetType()]
	if !ok {
		slog.Debug("Did not find handler for event type", "event_type", event.GetType())
		return nil, ErrNoHandlerForEvent
	}

	parsedEvent, err := handler(event)
	if err != nil {
		return nil, err
	}

	return parsedEvent, nil
}
