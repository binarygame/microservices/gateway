package events

import (
	"math/rand"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/oklog/ulid/v2"
	questionTypes "gitlab.com/binarygame/microservices/questions/pkg/models/question_types"
	models "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

/*
>>>>
================
BENCHMARKING
================
>>>>
*/

const (
	numberOfQuestionsInTestSet = 10
	roomIDSize                 = 4
)

var questionGenerators = []func(float32) models.Question{
	questionTypes.NewDecimalToBinary,
	questionTypes.NewBinaryToDecimal,
}

func testQuestion() models.Question {
	n := rand.Intn(len(questionGenerators))
	f := questionGenerators[n]
	return f(1.0)
}

func testQuestions() []models.Question {
	questions := make([]models.Question, numberOfQuestionsInTestSet)
	for i := 0; i < numberOfQuestionsInTestSet; i++ {
		questions[i] = testQuestion()
	}

	return questions
}

func sPtr(s string) *string {
	return &s
}

var letterRunes = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789")

func RandRoomID() string {
	b := make([]rune, roomIDSize)
	for i := range b {
		b[i] = letterRunes[rand.Intn(roomIDSize)]
	}
	return string(b)
}

func testBaseEvent(t EventType) *BaseEvent {
	return &BaseEvent{
		ID:          ulid.Make().String(),
		Timestamp:   time.Now(),
		Type:        t,
		RoomID:      RandRoomID(),
		Description: sPtr("test description for event"),
	}
}

var testEvents = map[string]Event{
	"RoomStarting": &RoomStartingEvent{
		BaseEvent: *testBaseEvent(RoomStarting),
		StartsIn:  time.Second * 5,
		QuestionSet: models.QuestionSet{
			ID:        uuid.New().String(),
			Level:     models.Easy,
			Questions: testQuestions(),
		},
	},
	"RoomPlaying":     testBaseEvent(RoomPlaying),
	"RoomEnding":      testBaseEvent(RoomEnding),
	"UserBanned":      testBaseEvent(UserBanned),
	"UserLeft":        testBaseEvent(UserLeft),
	"UserJoined":      testBaseEvent(UserJoined),
	"UserHostChanged": testBaseEvent(UserHostChanged),
}

var eventTypeString = map[EventType]string{
	RoomStarting:    "RoomStarting",
	RoomEnding:      "RoomEnding",
	RoomPlaying:     "RoomPlaying",
	UserLeft:        "UserLeft",
	UserJoined:      "UserJoined",
	UserBanned:      "UserBanned",
	UserHostChanged: "UserHostChanged",
}

func BenchmarkTranslateEventToProto(b *testing.B) {
	for n, v := range testEvents {
		b.Run(n, func(b *testing.B) {
			TranslateEventToProto(v)
		})
	}
}

/*
>>>>
================
TODO: TESTING
================
>>>>
*/
