package events

import (
	"time"

	guessesModel "gitlab.com/binarygame/microservices/guesses/pkg/models"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

// EventType represents the type of event
type EventType int

const (
	UnknownEventType EventType = iota

	/* Rooms microservice */

	RoomStarting
	RoomPlaying
	RoomEnding
	UserHostChanged
	UserJoined
	UserLeft
	UserLostConnection
	UserBanned
	UserUnbanned

	/* Guesses microservice */

	GuessSubmitted
	LeaderboardUpdated
)

// Event interface definition
type Event interface {
	GetID() string
	GetTimestamp() time.Time
	GetType() EventType
	GetDescription() *string
	GetRoomID() string
}

// BaseEvent struct with embedded fields
type BaseEvent struct {
	Timestamp   time.Time
	Description *string
	ID          string
	RoomID      string
	Type        EventType
}

// Implement the Event interface methods for BaseEvent
func (e *BaseEvent) GetID() string {
	return e.ID
}

func (e *BaseEvent) GetTimestamp() time.Time {
	return e.Timestamp
}

func (e *BaseEvent) GetType() EventType {
	return e.Type
}

func (e *BaseEvent) GetDescription() *string {
	return e.Description
}

func (e *BaseEvent) GetRoomID() string {
	return e.RoomID
}

/*
  Room user event
*/

type UserEvent struct {
	BaseEvent
	UserId string
}

func (e *UserEvent) GetUserId() string {
	return e.UserId
}

/*
  Room user joined event
  Treated differently to give user data to the frontend
*/

type UserJoinedEvent struct {
	UserEvent
	Nickname string
	Emoji    string
}

func (e *UserJoinedEvent) GetNickname() string {
	return e.Nickname
}

func (e *UserJoinedEvent) GetEmoji() string {
	return e.Emoji
}

/*
  RoomStartingEvent
*/

type RoomStartingEvent struct {
	BaseEvent
	QuestionSet questionsModel.QuestionSet
	StartsIn    time.Duration
}

func (e *RoomStartingEvent) GetQuestionSet() questionsModel.QuestionSet {
	return e.QuestionSet
}

func (e *RoomStartingEvent) GetStartsIn() time.Duration {
	return e.StartsIn
}

/*
  GuessEvent
*/

type GuessEvent struct {
	UserID     string
	QuestionID string
	BaseEvent
	Correct bool
}

func (e *GuessEvent) GetUserID() string {
	return e.UserID
}

func (e *GuessEvent) GetQuestionID() string {
	return e.QuestionID
}

func (e *GuessEvent) GetCorrect() bool {
	return e.Correct
}

/*
  LeaderboardUpdatedEvent
*/

type LeaderboardUpdatedEvent struct {
	BaseEvent
	Leaderboard guessesModel.Leaderboard
}

func (e *LeaderboardUpdatedEvent) GetLeaderboard() guessesModel.Leaderboard {
	return e.Leaderboard
}
