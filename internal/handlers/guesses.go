package handlers

import (
	"context"
	"log/slog"
	"net/http"
	"strconv"
	"time"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/constants"
	guessesv1 "gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1"
	"gitlab.com/binarygame/microservices/guesses/pkg/gen/connectrpc/guesses/v1/guessesv1connect"
	"gitlab.com/binarygame/microservices/guesses/pkg/models"
	"google.golang.org/protobuf/types/known/durationpb"
)

type guessesHandler struct {
	guessesClient guessesv1connect.GuessesServiceClient
	eventsClient  guessesv1connect.EventServiceClient
	roomsHandler  RoomsHandler

	logger    *slog.Logger
	telemetry telemetry.TelemetryService
}

type GuessesHandler interface {
	SubmitGuess(ctx context.Context, roomId string, questionId string, isCorrect bool, timestamp time.Duration) error
	SubscribeToRoom(ctx context.Context, roomId string) (<-chan models.Event, error)
}

func NewGuessesHandler(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) GuessesHandler {
	guessesServiceAddr := bingameutils.GetGuessesServiceAddress()
	guessesClient := guessesv1connect.NewGuessesServiceClient(
		http.DefaultClient, // TODO: Check if this can be altered to improve anything
		"http://"+guessesServiceAddr,
		connect.WithInterceptors(clientInterceptor),
	)
	eventsClient := guessesv1connect.NewEventServiceClient(
		http.DefaultClient,
		"http://"+guessesServiceAddr,
		connect.WithInterceptors(clientInterceptor),
	)
	roomsHandler := NewRoomsHandler(logger, ts, clientInterceptor)

	return &guessesHandler{
		logger:        logger,
		guessesClient: guessesClient,
		eventsClient:  eventsClient,
		roomsHandler:  roomsHandler,
		telemetry:     ts,
	}
}

func (g *guessesHandler) SubmitGuess(ctx context.Context, roomID string, questionID string, isCorrect bool, timeTaken time.Duration) error {
	span := g.telemetry.Record(ctx, "SubmitGuess", map[string]string{
		"roomId":     roomID,
		"questionId": questionID,
		"isCorrect":  strconv.FormatBool(isCorrect),
		"timeTaken":  timeTaken.String(),
		"userId":     bingameutils.GetUserIdFromCtx(ctx),
	})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	logger := g.logger.With(slog.Group("input",
		slog.String("room_id", roomID),
		slog.String("question_id", questionID),
		slog.Bool("is_correct", isCorrect),
		slog.Duration("time_taken", timeTaken),
	))

	logger.Debug("SubmitGuess called")

	userID := bingameutils.GetUserIdFromCtx(ctx)

	// Deny request if room is not currently playing a match
	isPlaying, err := g.roomsHandler.IsRoomPlaying(ctx, roomID)
	if err != nil {
		logger.Error("Could not check if room status is 'playing'", "error", err.Error())
		return err
	}
	if !isPlaying {
		logger.Info("Tried to submit guess to a match that is not playing")
		return constants.ErrMatchNotPlaying
	}

	req := &connect.Request[guessesv1.SubmitGuessRequest]{
		Msg: &guessesv1.SubmitGuessRequest{
			UserId:     userID,
			RoomId:     roomID,
			QuestionId: questionID,
			IsCorrect:  isCorrect,
			TimeTaken:  durationpb.New(timeTaken),
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err = g.guessesClient.SubmitGuess(ctx, req)
	if err != nil {
		logger.Error("Error in SubmitGuesses from guesses", "error", err.Error(), "code", connect.CodeOf(err).String())
		return err
	}

	return nil
}

// This function does not check user auth, be careful
func (g *guessesHandler) SubscribeToRoom(ctx context.Context, roomId string) (<-chan models.Event, error) {
	span := g.telemetry.Record(ctx, "SubscribeToRoom", map[string]string{"roomId": roomId})
	defer span.End()

	req := &connect.Request[guessesv1.SubscribeGuessesEventsRequest]{
		Msg: &guessesv1.SubscribeGuessesEventsRequest{RoomId: roomId},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	stream, err := g.eventsClient.SubscribeGuessesEvents(ctx, req)
	if err != nil {
		g.logger.Error("Error in SubscribeToRoom", "error", err.Error())
		return nil, err
	}

	ch := make(chan models.Event)

	// TODO: make sure this is treated correctly when the ctx ends
	go func() {
		defer close(ch)
		for stream.Receive() {
			protoEvent := stream.Msg().Data
			event, err := models.ConvertProtoEvent(protoEvent)
			if err != nil {
				g.logger.Error("Error converting proto event to model event",
					"error", err.Error(),
					"proto_event_id", protoEvent.EventId,
				)
			}
			ch <- event
		}

		if err := stream.Err(); err != nil && err != context.Canceled {
			g.logger.Error("SubscribeToRoom stream ended in error", "error", err.Error())
		} else {
			g.logger.Info("SubscribeToRoom stream ended without errors")
		}
	}()

	return ch, nil
}
