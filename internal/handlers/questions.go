package handlers

import (
	"context"
	"log/slog"
	"net/http"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	questionsv1 "gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1"
	"gitlab.com/binarygame/microservices/questions/pkg/gen/connectrpc/questions/v1/questionsv1connect"
	questions "gitlab.com/binarygame/microservices/questions/pkg/models"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
)

type questionsHandler struct {
	client    questionsv1connect.QuestionsServiceClient
	logger    *slog.Logger
	telemetry telemetry.TelemetryService
}

type QuestionsHandler interface {
	GetQuestionSet(ctx context.Context, questionSetID string) (*questionsModel.QuestionSet, error)
}

func NewQuestionsHandler(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) QuestionsHandler {
	client := questionsv1connect.NewQuestionsServiceClient(
		http.DefaultClient, // TODO: Check if this can be altered to improve anything
		"http://"+bingameutils.GetQuestionsServiceAddress(),
		connect.WithInterceptors(clientInterceptor),
	)
	return &questionsHandler{
		client:    client,
		logger:    logger,
		telemetry: ts,
	}
}

// No authentication, be careful!
func (q *questionsHandler) GetQuestionSet(ctx context.Context, questionSetID string) (*questionsModel.QuestionSet, error) {
	span := q.telemetry.Record(ctx, "GetQuestionSet", map[string]string{
		"questionSetId": questionSetID,
		"userId":        bingameutils.GetUserIdFromCtx(ctx),
	})
	defer span.End()

	logger := q.logger.With(slog.Group("input",
		slog.String("question_set_id", questionSetID),
		slog.String("user_id", bingameutils.GetUserIdFromCtx(ctx)),
	))

	logger.Debug("GetQuestionSet called!")

	req := &connect.Request[questionsv1.GetQuestionSetRequest]{
		Msg: &questionsv1.GetQuestionSetRequest{
			QuestionSetId: questionSetID,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := q.client.GetQuestionSet(ctx, req)
	if err != nil {
		logger.Error("Failed to get question set from id", "error", err.Error())
		return nil, err
	}

	questionSet, err := questions.ConvertProtoQuestionSet(resp.Msg.QuestionSet)
	if err != nil {
		logger.Error("Failed to convert question from protobuf message to model type", "error", err.Error())
		return nil, err
	}

	return questionSet, nil
}
