package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"strconv"
	"time"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/constants"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	roomsv1 "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1"
	roomsv1connect "gitlab.com/binarygame/microservices/rooms/pkg/gen/connectrpc/rooms/v1/roomsv1connect"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
	userModel "gitlab.com/binarygame/microservices/users/pkg/models"
	"google.golang.org/protobuf/types/known/durationpb"
)

type roomsHandler struct {
	roomsClient  roomsv1connect.RoomsServiceClient
	eventsClient roomsv1connect.EventServiceClient
	usersHandler UsersHandler
	logger       *slog.Logger
	telemetry    telemetry.TelemetryService
}

type RoomsHandler interface {
	GetRoom(ctx context.Context, roomId string) (*model.Room, error)
	CheckIfRoomExists(ctx context.Context, roomID string) (bool, error)
	GetUsersInRoom(ctx context.Context, roomId string) ([]string, error)
	CreateRoom(ctx context.Context, hostId, name string, maxPlayers int32) (*CreateRoomData, error)
	EditRoom(ctx context.Context, roomId, roomName string, maxPlayers uint32, playingDuration *time.Duration, level questionsModel.QuestionSetLevel, shuffle bool) error
	Join(ctx context.Context, roomId string) (roomData *RoomData, wasInRoomBefore bool, repeatAt time.Time, err error)
	Leave(ctx context.Context, roomId string) error
	BanUserFromRoom(ctx context.Context, userId, roomId string) error
	UnbanUserFromRoom(ctx context.Context, userId, roomId string) error
	GetQuestionCount(ctx context.Context, roomId string) (int32, error)
	StartMatch(ctx context.Context, roomId string) error
	SubscribeToAll(ctx context.Context) (<-chan model.Event, error)
	SubscribeToRoom(ctx context.Context, roomId string) (<-chan model.Event, error)
	IsRoomPlaying(ctx context.Context, roomId string) (bool, error)
}

type CreateRoomData struct {
	RoomId string
}

type RoomData struct {
	Name          string
	HostId        string
	Users         []userModel.User
	BannedUsers   []userModel.User
	Status        model.RoomStatus
	MaxPlayers    uint32
	QuestionCount uint32
}

func NewRoomsHandler(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) RoomsHandler {
	roomsServiceAddr := bingameutils.GetRoomsServiceAddress()
	roomsClient := roomsv1connect.NewRoomsServiceClient(
		http.DefaultClient,
		"http://"+roomsServiceAddr,
		connect.WithInterceptors(clientInterceptor),
	)

	eventsClient := roomsv1connect.NewEventServiceClient(
		http.DefaultClient,
		"http://"+roomsServiceAddr,
		connect.WithInterceptors(clientInterceptor),
	)

	usersHandler := NewUsersHandler(logger, ts, clientInterceptor)

	return &roomsHandler{
		logger:       logger,
		usersHandler: usersHandler,
		roomsClient:  roomsClient,
		eventsClient: eventsClient,
		telemetry:    ts,
	}
}

func (r *roomsHandler) GetRoom(ctx context.Context, roomId string) (*model.Room, error) {
	span := r.telemetry.Record(ctx, "GetRoom", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.GetRoomRequest]{
		Msg: &roomsv1.GetRoomRequest{
			RoomId: roomId,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.GetRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in GetRoom", "error", err.Error())
		return nil, err
	}

	status, err := model.ConvertProtoStatus(resp.Msg.GetRoomStatus())
	if err != nil {
		r.logger.Error("Could not convert room status in GetRoom",
			"error", err.Error(),
			"proto_status", int32(resp.Msg.GetRoomStatus()),
		)
		return nil, err
	}

	return &model.Room{
		ID:            roomId,
		Name:          resp.Msg.Name,
		HostID:        resp.Msg.HostId,
		MaxPlayers:    uint32(resp.Msg.MaxPlayers),
		QuestionCount: uint32(resp.Msg.QuestionCount),
		Status:        status,
	}, nil
}

func (r *roomsHandler) CheckIfRoomExists(ctx context.Context, roomID string) (bool, error) {
	span := r.telemetry.Record(ctx, "CheckIfRoomExists", map[string]string{"roomId": roomID})
	defer span.End()

	req := &connect.Request[roomsv1.CheckIfRoomExistsRequest]{
		Msg: &roomsv1.CheckIfRoomExistsRequest{RoomId: roomID},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.CheckIfRoomExists(ctx, req)
	if err != nil {
		r.logger.Error("Could not check if room exists", "error", err.Error())
		return false, err
	}

	return resp.Msg.Exists, nil
}

func (r *roomsHandler) GetUsersInRoom(ctx context.Context, roomId string) ([]string, error) {
	span := r.telemetry.Record(ctx, "GetUsersInRoom", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.GetUsersInRoomRequest]{
		Msg: &roomsv1.GetUsersInRoomRequest{RoomId: roomId},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.GetUsersInRoom(ctx, req)
	if err != nil {
		r.logger.Error("Could not get users from room", "error", err.Error())
		return nil, err
	}

	return resp.Msg.UserIds, nil
}

func (r *roomsHandler) CreateRoom(ctx context.Context, hostId, name string, maxPlayers int32) (*CreateRoomData, error) {
	span := r.telemetry.Record(ctx, "CreateRoom", map[string]string{
		"hostId":     hostId,
		"name":       name,
		"maxPlayers": strconv.Itoa(int(maxPlayers)),
	})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.CreateRoomRequest]{
		Msg: &roomsv1.CreateRoomRequest{
			HostId:     bingameutils.GetUserIdFromCtx(ctx),
			Name:       name,
			MaxPlayers: uint32(maxPlayers),
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.CreateRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in CreateRoom", "error", err.Error())
		return nil, err
	}

	return &CreateRoomData{
		RoomId: resp.Msg.Data.RoomId,
	}, nil
}

func (r *roomsHandler) EditRoom(ctx context.Context, roomId, roomName string, maxPlayers uint32, playingDuration *time.Duration, level questionsModel.QuestionSetLevel, shuffle bool) error {
	span := r.telemetry.Record(ctx, "Edit",
		map[string]string{
			"roomId":          roomId,
			"roomName":        roomName,
			"maxPlayers":      fmt.Sprint(maxPlayers),
			"playingDuration": fmt.Sprint(playingDuration),
			"level":           level.String(),
			"shuffle":         strconv.FormatBool(shuffle),
		})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	var protoDiff roomsv1.DifficultyLevel

	switch level {
	case questionsModel.Easy:
		protoDiff = roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY
	case questionsModel.Medium:
		protoDiff = roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM
	case questionsModel.Hard:
		protoDiff = roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD
	}

	req := connect.NewRequest(&roomsv1.EditRoomRequest{
		RoomId:        roomId,
		Name:          roomName,
		MaxPlayers:    maxPlayers,
		MatchDuration: durationpb.New(*playingDuration),
		Difficulty:    protoDiff,
		Shuffle:       shuffle,
	})

	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := r.roomsClient.EditRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in CreateRoom", "error", err.Error())
		return err
	}

	return nil
}

func (r *roomsHandler) Join(ctx context.Context, roomId string) (*RoomData, bool, time.Time, error) {
	const repeatAtLeniency = 3 * time.Second

	span := r.telemetry.Record(ctx, "Join", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, false, time.Time{}, constants.ErrUserAuthFailed
	}

	userId := bingameutils.GetUserIdFromCtx(ctx)
	req := connect.NewRequest(&roomsv1.RegisterUserInRoomRequest{
		RoomId: roomId,
		UserId: userId,
	})
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.RegisterUserInRoom(ctx, req)
	if err != nil {
		r.logger.Error("Failed to register user in room", "error", err)
	}

	// The user should repeat the call at this timestamp
	repeatAt := resp.Msg.ExpiresAt.AsTime().Add(-repeatAtLeniency)

	if !resp.Msg.WasInRoomBefore {
		room, err := r.GetRoom(ctx, roomId)
		if err != nil {
			r.logger.Error("Failed to get room information", "error", err, "roomId", roomId)
			return nil, false, time.Time{}, err
		}

		userIds, err := r.GetUsersInRoom(ctx, roomId)
		if err != nil {
			r.logger.Error("Failed to get room user list", "error", err, "roomId", roomId)
			return nil, false, time.Time{}, err
		}

		users, err := r.usersHandler.GetUsers(ctx, userIds)
		if err != nil {
			r.logger.Error("Failed to get users from user id list", "error", err)
			return nil, false, time.Time{}, err
		}

		bannedUsersIds, err := r.GetBannedUsersInRoom(ctx, roomId)
		if err != nil {
			r.logger.Error("Failed to get room banned user list", "error", err)
			return nil, false, time.Time{}, err
		}

		bannedUsers, err := r.usersHandler.GetUsers(ctx, bannedUsersIds)
		if err != nil {
			r.logger.Error("Failed to get users from banned user id list", "error", err)
			return nil, false, time.Time{}, err
		}

		return &RoomData{
			Users:         users,
			BannedUsers:   bannedUsers,
			Name:          room.Name,
			HostId:        room.HostID,
			QuestionCount: room.QuestionCount,
			Status:        room.Status,
			MaxPlayers:    room.MaxPlayers,
		}, resp.Msg.WasInRoomBefore, repeatAt, nil
	}

	return nil, resp.Msg.WasInRoomBefore, repeatAt, nil
}

func (r *roomsHandler) Leave(ctx context.Context, roomId string) error {
	span := r.telemetry.Record(ctx, "Leave", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	userId := bingameutils.GetUserIdFromCtx(ctx)

	req := connect.NewRequest(&roomsv1.RemoveUserFromRoomRequest{
		RoomId: roomId,
		UserId: userId,
	})
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := r.roomsClient.RemoveUserFromRoom(ctx, req)
	if err != nil {
		return err
	}

	return nil
}

func (r *roomsHandler) BanUserFromRoom(ctx context.Context, userId, roomId string) error {
	span := r.telemetry.Record(ctx, "BanUserFromRoom", map[string]string{
		"userId": userId,
		"roomId": roomId,
	})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.BanUserFromRoomRequest]{
		Msg: &roomsv1.BanUserFromRoomRequest{
			UserId: userId,
			RoomId: roomId,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := r.roomsClient.BanUserFromRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in BanUserFromRoom", "error", err.Error())
		return err
	}
	return nil
}

func (r *roomsHandler) UnbanUserFromRoom(ctx context.Context, userId, roomId string) error {
	span := r.telemetry.Record(ctx, "UnbanUserFromRoom", map[string]string{
		"userId": userId,
		"roomId": roomId,
	})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	req := connect.NewRequest(&roomsv1.UnbanUserFromRoomRequest{
		UserId: userId,
		RoomId: roomId,
	})
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := r.roomsClient.UnbanUserFromRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in UnbanUserFromRoom", "error", err.Error())
		return err
	}
	return nil
}

func (r *roomsHandler) GetBannedUsersInRoom(ctx context.Context, roomId string) ([]string, error) {
	span := r.telemetry.Record(ctx, "GetBannedUsers", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := connect.NewRequest(&roomsv1.GetBannedUsersFromRoomRequest{RoomId: roomId})

	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.GetBannedUsersFromRoom(ctx, req)
	if err != nil {
		r.logger.Error("Could not get banned users from room", "error", err.Error())
		return nil, err
	}

	return resp.Msg.UserIds, nil
}

func (r *roomsHandler) GetQuestionCount(ctx context.Context, roomId string) (int32, error) {
	span := r.telemetry.Record(ctx, "GetQuestionCount", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return 0, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.GetQuestionCountRequest]{
		Msg: &roomsv1.GetQuestionCountRequest{
			RoomId: roomId,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.GetQuestionCount(ctx, req)
	if err != nil {
		r.logger.Error("Error in GetQuestionCount", "error", err.Error())
		return 0, err
	}

	return resp.Msg.Data.QuestionCount, nil
}

func (r *roomsHandler) StartMatch(ctx context.Context, roomId string) error {
	span := r.telemetry.Record(ctx, "StartMatch", map[string]string{
		"roomId": roomId,
	})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.StartMatchRequest]{
		Msg: &roomsv1.StartMatchRequest{
			RoomId: roomId,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := r.roomsClient.StartMatch(ctx, req)
	if err != nil {
		r.logger.Error("Error in StartMatch", "error", err.Error())
		return err
	}

	return nil
}

func (r *roomsHandler) SubscribeToAll(ctx context.Context) (<-chan model.Event, error) {
	span := r.telemetry.Record(ctx, "SubscribeToAll", nil)
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.SubscribeAllRoomEventsRequest]{
		Msg: &roomsv1.SubscribeAllRoomEventsRequest{},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	stream, err := r.eventsClient.SubscribeAllRoomEvents(ctx, req)
	if err != nil {
		r.logger.Error("Error in SubscribeToAll", "error", err.Error())
		return nil, err
	}

	ch := make(chan model.Event)

	go func() {
		defer close(ch)
		for stream.Receive() {
			protoEvent := stream.Msg().Data
			event, err := model.ConvertProtoEvent(protoEvent)
			if err != nil {
				r.logger.Error("Error converting proto event to model event",
					"error", err.Error(),
					"proto_event_id", protoEvent.EventId,
				)
			}
			ch <- event
		}

		if err := stream.Err(); err != nil {
			r.logger.Error("SubscribeToAll stream ended in error", "error", err.Error())
		} else {
			r.logger.Info("SubscribeToAll stream ended without errors")
		}
	}()

	return ch, nil
}

func (r *roomsHandler) SubscribeToRoom(ctx context.Context, roomId string) (<-chan model.Event, error) {
	span := r.telemetry.Record(ctx, "SubscribeToRoom", map[string]string{"roomId": roomId})
	defer span.End()

	req := &connect.Request[roomsv1.SubscribeRoomEventsRequest]{
		Msg: &roomsv1.SubscribeRoomEventsRequest{RoomId: roomId},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	stream, err := r.eventsClient.SubscribeRoomEvents(ctx, req)
	if err != nil {
		r.logger.Error("Error in SubscribeToRoom", "error", err.Error())
		return nil, err
	}

	ch := make(chan model.Event)

	go func() {
		defer close(ch)
		for stream.Receive() {
			protoEvent := stream.Msg().Data
			e, _ := json.Marshal(protoEvent)
			r.logger.Debug("raw protoEvent JSON", "protoEvent", e)
			event, err := model.ConvertProtoEvent(protoEvent)
			if err != nil {
				r.logger.Error("Error converting proto event to model event",
					"error", err.Error(),
					"proto_event_id", protoEvent.EventId,
				)
				continue
			}
			ch <- event
		}

		if err := stream.Err(); err != nil && err != context.Canceled {
			r.logger.Error("SubscribeToRoom stream ended in error", "error", err.Error())
		} else {
			r.logger.Info("SubscribeToRoom stream ended without errors")
		}
	}()

	return ch, nil
}

func (r *roomsHandler) IsRoomPlaying(ctx context.Context, roomId string) (bool, error) {
	span := r.telemetry.Record(ctx, "IsRoomPlaying", map[string]string{"roomId": roomId})
	defer span.End()

	if !bingameutils.IsCtxAuthValid(ctx) {
		return false, constants.ErrUserAuthFailed
	}

	req := &connect.Request[roomsv1.GetRoomRequest]{
		Msg: &roomsv1.GetRoomRequest{
			RoomId: roomId,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	resp, err := r.roomsClient.GetRoom(ctx, req)
	if err != nil {
		r.logger.Error("Error in IsRoomPlaying", "error", err.Error())
		return false, err
	}

	status, err := model.ConvertProtoStatus(resp.Msg.GetRoomStatus())
	if err != nil {
		r.logger.Error("Could not convert room status in IsRoomPlaying",
			"error", err.Error(),
			"proto_status", int32(resp.Msg.GetRoomStatus()),
		)
		return false, err
	}

	return status == model.Playing, nil
}
