package handlers

import (
	"context"
	"log/slog"
	"net/http"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/constants"
	usersv1 "gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1"
	"gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1/usersv1connect"
	"gitlab.com/binarygame/microservices/users/pkg/models"
)

type usersHandler struct {
	client    usersv1connect.UsersServiceClient
	logger    *slog.Logger
	telemetry telemetry.TelemetryService
}

type UsersHandler interface {
	GetValidEmojis(ctx context.Context) (EmojiList, error)
	NewUser(ctx context.Context, nickname, emoji string) (*NewUserData, error)
	EditSelf(ctx context.Context, nickname, emoji string) error
	GetUsers(ctx context.Context, userIds []string) ([]models.User, error)
	GetUser(ctx context.Context, userId string) (*models.User, error)
}

type EmojiList []string

type NewUserData struct {
	Id         string
	PrivateKey string
}

func NewUsersHandler(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) UsersHandler {
	client := usersv1connect.NewUsersServiceClient(
		http.DefaultClient, // TODO: Check if this can be altered to improve anything
		"http://"+bingameutils.GetUserServiceAddress(),
		connect.WithInterceptors(clientInterceptor),
	)
	return &usersHandler{
		client:    client,
		logger:    logger,
		telemetry: ts,
	}
}

func (u *usersHandler) GetValidEmojis(ctx context.Context) (EmojiList, error) {
	span := u.telemetry.Record(ctx, "GetValidEmojis", nil)
	defer span.End()

	req := connect.NewRequest(&usersv1.GetAllowedEmojisRequest{})
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	r, err := u.client.GetAllowedEmojis(ctx, req)
	if err != nil {
		u.logger.Error("Error in GetValidEmojis", "error", err)
		return nil, err
	}

	return r.Msg.Data, nil
}

func (u *usersHandler) NewUser(ctx context.Context, nickname, emoji string) (*NewUserData, error) {
	span := u.telemetry.Record(ctx, "NewUser", map[string]string{
		"nickname": nickname,
		"emoji":    emoji,
	})
	defer span.End()

	r, err := u.client.AddUser(ctx, &connect.Request[usersv1.AddUserRequest]{
		Msg: &usersv1.AddUserRequest{
			Nickname: nickname,
			Emoji:    emoji,
		},
	})
	if err != nil {
		u.logger.Error("Error in NewUser", "error", err)
		return nil, err
	}

	u.logger.Info("New user added", "response", r.Msg)

	return &NewUserData{
		Id:         r.Msg.Data.Id,
		PrivateKey: r.Msg.Data.PrivateKey,
	}, nil
}

func (u *usersHandler) EditSelf(ctx context.Context, nickname, emoji string) error {
	span := u.telemetry.Record(ctx, "EditSelf", map[string]string{
		"nickname": nickname,
		"emoji":    emoji,
	})
	defer span.End()
	if !bingameutils.IsCtxAuthValid(ctx) {
		return constants.ErrUserAuthFailed
	}

	req := &connect.Request[usersv1.EditUserRequest]{
		Msg: &usersv1.EditUserRequest{
			UserId:   bingameutils.GetUserIdFromCtx(ctx),
			Nickname: nickname,
			Emoji:    emoji,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	_, err := u.client.EditUser(ctx, req)
	if err != nil {
		u.logger.Error("Error in EditSelf", "error", err.Error())
		return err
	}

	return nil
}

func (u *usersHandler) GetUsers(ctx context.Context, userIds []string) ([]models.User, error) {
	if !bingameutils.IsCtxAuthValid(ctx) {
		return nil, constants.ErrUserAuthFailed
	}

	req := &connect.Request[usersv1.GetUsersRequest]{
		Msg: &usersv1.GetUsersRequest{
			UserIds: userIds,
		},
	}
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	r, err := u.client.GetUsers(ctx, req)
	if err != nil {
		u.logger.Error("Error in GetUsers", "error", err.Error())
		return nil, err
	}

	users := make([]models.User, len(r.Msg.Users))

	i := 0
	for id, user := range r.Msg.Users {
		users[i] = models.User{
			ID:       id,
			Nickname: user.Nickname,
			Emoji:    user.Emoji,
		}

		i++
	}

	return users, nil
}

// CAUTION! No Auth required!
func (u *usersHandler) GetUser(ctx context.Context, userId string) (*models.User, error) {
	req := connect.NewRequest(&usersv1.GetUserRequest{
		UserId: userId,
	})
	bingameutils.AddAuthToHeaderFromCtx(ctx, req.Header())

	r, err := u.client.GetUser(ctx, req)
	if err != nil {
		u.logger.Error("Error in GetUsers", "error", err.Error())
		return nil, err
	}

	user := models.User{
		ID:       userId,
		Nickname: r.Msg.Nickname,
		Emoji:    r.Msg.Emoji,
	}

	return &user, nil
}
