package middleware

import (
	"log"
	"net/http"
	"time"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		// Call the next handler in the chain
		next.ServeHTTP(w, r)

		// Log the request
		log.Printf("[%s] %s %s %s\n", r.Method, r.RequestURI, r.RemoteAddr, time.Since(start))
	})
}
