package service

import (
	"context"
	"log/slog"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers/events"
	eventsv1 "gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/events/v1"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/events/v1/eventsv1connect"
)

type EventsServer struct {
	eventsv1connect.UnimplementedEventsServiceHandler
	handler handlers.EventsHandler
	logger  *slog.Logger
}

func NewEventsServer(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) *EventsServer {
	return &EventsServer{
		handler: handlers.NewEventsHandler(logger, ts, clientInterceptor),
		logger:  logger,
	}
}

func (es *EventsServer) SubscribeEvents(ctx context.Context, req *connect.Request[eventsv1.SubscribeEventsRequest], stream *connect.ServerStream[eventsv1.SubscribeEventsResponse]) error {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, req.Header())
	roomId := req.Msg.RoomId
	userId := bingameutils.GetUserIdFromCtx(ctx)
	logger := es.logger.With(slog.Group("input",
		"room_id", roomId,
		"user_id", userId,
	))

	eventCh, err := es.handler.SubscribeToRoom(ctx, roomId)
	if err != nil {
		logger.Error("Failed to subscribe to room", "error", err, "room_id", roomId)
		return err
	}

	logger.Info("Subscribed to room events")

	for {
		select {
		case <-ctx.Done():
			logger.Info("Context done, ending subscription")
			return ctx.Err()
		case event, ok := <-eventCh:
			if !ok {
				logger.Info("Event channel closed, ending subscription")
				return nil
			}

			parsedEvent, err := events.TranslateEventToProto(event)
			if err != nil {
				logger.Error("Could not translate event to protobuf format!", "error", err.Error())
				continue
			}

			res := &connect.Response[eventsv1.SubscribeEventsResponse]{
				Msg: &eventsv1.SubscribeEventsResponse{
					Event: parsedEvent,
				},
			}
			if err := stream.Send(res.Msg); err != nil {
				logger.Error("Failed to send event", "error", err)
				return err
			}

			logger.Debug("Sent event", "eventType", parsedEvent.GetEventType())
		}
	}
}
