package service

import (
	"context"
	"log/slog"

	"connectrpc.com/connect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers"
	guessesv1 "gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/guesses/v1"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/guesses/v1/guessesv1connect"
)

// GuessesServer implements the GuessesService interface.
type GuessesServer struct {
	guessesv1connect.UnimplementedGuessesServiceHandler
	handler handlers.GuessesHandler
	logger  *slog.Logger
}

func NewGuessesServer(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) *GuessesServer {
	return &GuessesServer{
		handler: handlers.NewGuessesHandler(logger, ts, clientInterceptor),
		logger:  logger,
	}
}

func (gs *GuessesServer) SubmitGuess(ctx context.Context, r *connect.Request[guessesv1.SubmitGuessRequest]) (*connect.Response[guessesv1.SubmitGuessResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	gs.logger.Debug("SubmitGuess called")

	err := gs.handler.SubmitGuess(ctx, r.Msg.RoomId, r.Msg.QuestionId, r.Msg.IsCorrect, r.Msg.TimeTaken.AsDuration())
	if err != nil {
		gs.logger.Error("Error in SubmitGuess handler", "error", err.Error())
		return nil, err
	}

	return connect.NewResponse(&guessesv1.SubmitGuessResponse{}), nil
}
