package service

import (
	"context"
	"log/slog"
	"time"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers"
	roomsv1 "gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/rooms/v1"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/rooms/v1/roomsv1connect"
	questionsModel "gitlab.com/binarygame/microservices/questions/pkg/models/questions"
	"gitlab.com/binarygame/microservices/rooms/pkg/model"
	"google.golang.org/protobuf/types/known/timestamppb"

	"connectrpc.com/connect"
)

// RoomsServer implements the RoomsService interface.
type RoomsServer struct {
	roomsv1connect.UnimplementedRoomsServiceHandler
	handler handlers.RoomsHandler
	logger  *slog.Logger
}

func NewRoomsServer(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) *RoomsServer {
	return &RoomsServer{
		handler: handlers.NewRoomsHandler(logger, ts, clientInterceptor),
		logger:  logger,
	}
}

// CreateRoom creates a new room in the system.
func (rs *RoomsServer) CreateRoom(ctx context.Context, r *connect.Request[roomsv1.CreateRoomRequest]) (*connect.Response[roomsv1.CreateRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("CreateRoom called")

	hostId := bingameutils.GetUserIdFromCtx(ctx)
	room, err := rs.handler.CreateRoom(ctx, hostId, r.Msg.Name, 10) // Assuming default maxPlayers and playingDuration values
	if err != nil {
		rs.logger.Error("Failed to create room", "error", err, "hostId", hostId)
		return nil, err
	}

	rs.logger.Info("Room created successfully", "roomId", room.RoomId, "hostId", hostId)
	return connect.NewResponse(&roomsv1.CreateRoomResponse{
		Data: &roomsv1.CreateRoomData{
			RoomId: room.RoomId,
		},
	}), nil
}

// EditRoom edits an existing room in the system.
func (rs *RoomsServer) EditRoom(ctx context.Context, r *connect.Request[roomsv1.EditRoomRequest]) (*connect.Response[roomsv1.EditRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("EditRoom called")

	var duration *time.Duration
	if r.Msg.MatchDuration != nil {
		d := r.Msg.GetMatchDuration().AsDuration()
		duration = &d
	}

	var level questionsModel.QuestionSetLevel
	switch r.Msg.Difficulty {
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_EASY:
		level = questionsModel.Easy
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_MEDIUM:
		level = questionsModel.Medium
	case roomsv1.DifficultyLevel_DIFFICULTY_LEVEL_HARD:
		level = questionsModel.Hard
	}

	err := rs.handler.EditRoom(ctx, r.Msg.RoomId, r.Msg.Name, r.Msg.MaxPlayers, duration, level, r.Msg.Shuffle)
	if err != nil {
		rs.logger.Error("Failed to edit room", "error", err)
		return nil, err
	}

	return connect.NewResponse(&roomsv1.EditRoomResponse{}), nil
}

func (rs *RoomsServer) CheckIfRoomExists(ctx context.Context, r *connect.Request[roomsv1.CheckIfRoomExistsRequest]) (*connect.Response[roomsv1.CheckIfRoomExistsResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("CheckIfRoomExists called", "roomID", r.Msg.RoomId)

	exists, err := rs.handler.CheckIfRoomExists(ctx, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to check if room exists", "error", err.Error(), "roomId", r.Msg.RoomId)
		return nil, err
	}

	return &connect.Response[roomsv1.CheckIfRoomExistsResponse]{
		Msg: &roomsv1.CheckIfRoomExistsResponse{
			Exists: exists,
		},
	}, nil
}

// JoinRoom allows an user to join a room.
// The user keeps pinging via the stream to keep connected.
func (rs *RoomsServer) JoinRoom(ctx context.Context, r *connect.Request[roomsv1.JoinRoomRequest]) (*connect.Response[roomsv1.JoinRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	rs.logger.Info("JoinRoom called", "roomId", r.Msg.RoomId)
	roomData, wasInRoomBefore, repeatAt, err := rs.handler.Join(ctx, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to join room", "error", err.Error(), "roomId", r.Msg.RoomId)
		return nil, err
	}

	if !wasInRoomBefore {
		var roomStatus roomsv1.RoomStatus
		switch roomData.Status {
		case model.Waiting:
			roomStatus = roomsv1.RoomStatus_ROOM_STATUS_WAITING
		case model.Starting:
			roomStatus = roomsv1.RoomStatus_ROOM_STATUS_STARTING
		case model.Playing:
			roomStatus = roomsv1.RoomStatus_ROOM_STATUS_PLAYING
		case model.Ending:
			roomStatus = roomsv1.RoomStatus_ROOM_STATUS_ENDING
		default:
			rs.logger.Warn("received an unhandled room status, check the code", "roomData.Status", roomData.Status)
			roomStatus = roomsv1.RoomStatus_ROOM_STATUS_UNSPECIFIED
		}

		users := make([]*roomsv1.User, len(roomData.Users))
		for i, user := range roomData.Users {
			users[i] = &roomsv1.User{
				UserId:   user.ID,
				Nickname: user.Nickname,
				Emoji:    user.Emoji,
			}
		}

		bannedUsers := make([]*roomsv1.User, len(roomData.BannedUsers))
		for i, bannedUser := range roomData.BannedUsers {
			bannedUsers[i] = &roomsv1.User{
				UserId:   bannedUser.ID,
				Nickname: bannedUser.Nickname,
				Emoji:    bannedUser.Emoji,
			}
		}
		return connect.NewResponse(&roomsv1.JoinRoomResponse{
			RepeatAt: timestamppb.New(repeatAt),
			Room: &roomsv1.RoomData{
				Name:          roomData.Name,
				HostId:        roomData.HostId,
				MaxPlayers:    int32(roomData.MaxPlayers),
				QuestionCount: int32(roomData.QuestionCount),
				RoomStatus:    roomStatus,
				Users:         users,
				BannedUsers:   bannedUsers,
			},
		}), nil
	} else {
		return connect.NewResponse(&roomsv1.JoinRoomResponse{
			RepeatAt: timestamppb.New(repeatAt),
		}), nil
	}
}

// LeaveRoom allows an user to leave a room
func (rs *RoomsServer) LeaveRoom(ctx context.Context, r *connect.Request[roomsv1.LeaveRoomRequest]) (*connect.Response[roomsv1.LeaveRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("LeaveRoom called", "roomId", r.Msg.RoomId)

	err := rs.handler.Leave(ctx, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to leave room", "error", err, "roomId", r.Msg.RoomId)
		return nil, err
	}

	return connect.NewResponse(&roomsv1.LeaveRoomResponse{}), nil
}

// BanUserFromRoom bans an user from a room.
func (rs *RoomsServer) BanUserFromRoom(ctx context.Context, r *connect.Request[roomsv1.BanUserFromRoomRequest]) (*connect.Response[roomsv1.BanUserFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("BanUserFromRoom called", "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)

	err := rs.handler.BanUserFromRoom(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to ban user from room", "error", err, "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)
		return nil, err
	}

	rs.logger.Info("User banned from room successfully", "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)
	return connect.NewResponse(&roomsv1.BanUserFromRoomResponse{}), nil
}

// BanUserFromRoom unbans a user from a room.
func (rs *RoomsServer) UnbanUserFromRoom(ctx context.Context, r *connect.Request[roomsv1.UnbanUserFromRoomRequest]) (*connect.Response[roomsv1.UnbanUserFromRoomResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("UnbanUserFromRoom called", "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)

	err := rs.handler.UnbanUserFromRoom(ctx, r.Msg.UserId, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to unban user from room", "error", err, "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)
		return nil, err
	}

	rs.logger.Info("User unbanned from room successfully", "userId", r.Msg.UserId, "roomId", r.Msg.RoomId)
	return connect.NewResponse(&roomsv1.UnbanUserFromRoomResponse{}), nil
}

// GetQuestionCount retrieves the number of questions in a room.
func (rs *RoomsServer) GetQuestionCount(ctx context.Context, r *connect.Request[roomsv1.GetQuestionCountRequest]) (*connect.Response[roomsv1.GetQuestionCountResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("GetQuestionCount called", "roomId", r.Msg.RoomId)

	count, err := rs.handler.GetQuestionCount(ctx, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to get question count", "error", err, "roomId", r.Msg.RoomId)
		return nil, err
	}

	rs.logger.Info("Retrieved question count successfully", "roomId", r.Msg.RoomId, "count", count)
	return connect.NewResponse(&roomsv1.GetQuestionCountResponse{
		Data: &roomsv1.QuestionCountData{
			QuestionCount: count,
		},
	}), nil
}

// StartMatch starts a new match in a room.
func (rs *RoomsServer) StartMatch(ctx context.Context, r *connect.Request[roomsv1.StartMatchRequest]) (*connect.Response[roomsv1.StartMatchResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	rs.logger.Info("StartMatch called", "roomId", r.Msg.RoomId)

	err := rs.handler.StartMatch(ctx, r.Msg.RoomId)
	if err != nil {
		rs.logger.Error("Failed to start match", "error", err, "roomId", r.Msg.RoomId)
		return nil, err
	}

	rs.logger.Info("Match started successfully", "roomId", r.Msg.RoomId)
	return connect.NewResponse(&roomsv1.StartMatchResponse{}), nil
}
