package service

import (
	"log/slog"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/events/v1/eventsv1connect"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/guesses/v1/guessesv1connect"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/rooms/v1/roomsv1connect"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/users/v1/usersv1connect"

	"connectrpc.com/connect"
	connectcors "connectrpc.com/cors"
	"connectrpc.com/grpcreflect"
	"connectrpc.com/otelconnect"
	"github.com/rs/cors"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

// withCORS adds CORS support to a Connect HTTP handler.
func withCORS(h http.Handler) http.Handler {
	allowedHeaders := connectcors.AllowedHeaders()
	allowedHeaders = append(allowedHeaders, "Authorization")

	middleware := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: connectcors.AllowedMethods(),
		AllowedHeaders: allowedHeaders,
		ExposedHeaders: connectcors.ExposedHeaders(),
	})
	return middleware.Handler(h)
}

func Start(ip string, port int, logger *slog.Logger, ts telemetry.TelemetryService) {
	// HTTP server
	mux := http.NewServeMux()

	clientInterceptors, err := otelconnect.NewInterceptor()
	if err != nil {
		logger.Error("Could not create otel connect interceptor", "error", err)
	}

	// Implementation specific handlers
	users := NewUsersServer(logger, ts, clientInterceptors)
	rooms := NewRoomsServer(logger, ts, clientInterceptors)
	events := NewEventsServer(logger, ts, clientInterceptors)
	guesses := NewGuessesServer(logger, ts, clientInterceptors)

	otelInterceptor, err := otelconnect.NewInterceptor()
	if err != nil {
		logger.Error("An error occurred in otelconnect.NewInterceptor", "error", err)
	}

	// For GRPC reflection
	reflector := grpcreflect.NewStaticReflector(
		roomsv1connect.RoomsServiceName,
		usersv1connect.UsersServiceName,
		eventsv1connect.EventsServiceName,
		guessesv1connect.GuessesServiceName,
	)

	usersPath, usersHandler := usersv1connect.NewUsersServiceHandler(users, connect.WithInterceptors(otelInterceptor))
	roomsPath, roomsHandler := roomsv1connect.NewRoomsServiceHandler(rooms, connect.WithInterceptors(otelInterceptor))
	eventsPath, eventsHandler := eventsv1connect.NewEventsServiceHandler(events, connect.WithInterceptors(otelInterceptor))
	guessesPath, guessesHandler := guessesv1connect.NewGuessesServiceHandler(guesses, connect.WithInterceptors(otelInterceptor))

	usersHandler = withCORS(usersHandler)
	roomsHandler = withCORS(roomsHandler)
	eventsHandler = withCORS(eventsHandler)
	guessesHandler = withCORS(guessesHandler)

	mux.Handle(usersPath, usersHandler)
	mux.Handle(roomsPath, roomsHandler)
	mux.Handle(eventsPath, eventsHandler)
	mux.Handle(guessesPath, guessesHandler)

	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	frontendUrl, _ := url.Parse("http://" + bingameutils.GetFrontendServiceAddress())
	proxy := httputil.NewSingleHostReverseProxy(frontendUrl)
	mux.Handle("/", proxy)
	address := ip + ":" + strconv.Itoa(port)
	logger.Info("Starting connect server", "ip", ip, "port", port)
	err = http.ListenAndServe(
		address,
		// Use h2c so we can serve HTTP/2 without TLS.
		h2c.NewHandler(mux, &http2.Server{}),
	)
	if err != nil {
		logger.Error("An error occurred in the web server", "error", err.Error())
	}
}
