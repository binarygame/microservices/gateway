package service

import (
	"context"
	"log/slog"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/gateway/internal/handlers"
	usersv1 "gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/users/v1"
	"gitlab.com/binarygame/microservices/gateway/pkg/gen/gateway/users/v1/usersv1connect"

	"connectrpc.com/connect"
)

type UsersServer struct {
	usersv1connect.UnimplementedUsersServiceHandler
	handler handlers.UsersHandler
	logger  *slog.Logger
}

func NewUsersServer(logger *slog.Logger, ts telemetry.TelemetryService, clientInterceptor connect.Interceptor) *UsersServer {
	return &UsersServer{
		handler: handlers.NewUsersHandler(logger, ts, clientInterceptor),
		logger:  logger,
	}
}

// GetAllowedEmojis returns a list of allowed emojis.
func (us *UsersServer) GetAllowedEmojis(ctx context.Context, r *connect.Request[usersv1.GetAllowedEmojisRequest]) (*connect.Response[usersv1.GetAllowedEmojisResponse], error) {
	us.logger.Info("GetAllowedEmojis called")

	emojis, err := us.handler.GetValidEmojis(ctx)
	if err != nil {
		us.logger.Error("Failed to get allowed emojis", "error", err)
		return nil, err
	}

	us.logger.Info("Retrieved allowed emojis successfully", "count", len(emojis))
	return connect.NewResponse(&usersv1.GetAllowedEmojisResponse{
		Data: emojis,
	}), nil
}

// Register creates a new user in the system.
func (us *UsersServer) Register(ctx context.Context, r *connect.Request[usersv1.RegisterRequest]) (*connect.Response[usersv1.RegisterResponse], error) {
	us.logger.Info("Register called", "nickname", r.Msg.Nickname)

	newUser, err := us.handler.NewUser(ctx, r.Msg.Nickname, r.Msg.Emoji)
	if err != nil {
		us.logger.Error("Failed to register new user", "error", err, "nickname", r.Msg.Nickname)
		return nil, err
	}

	us.logger.Info("User registered successfully", "userId", newUser.Id)
	return connect.NewResponse(&usersv1.RegisterResponse{
		Data: &usersv1.RegisterData{
			Id:         newUser.Id,
			PrivateKey: newUser.PrivateKey,
		},
	}), nil
}

// EditSelf modifies the authenticated user's details.
func (us *UsersServer) EditSelf(ctx context.Context, r *connect.Request[usersv1.EditSelfRequest]) (*connect.Response[usersv1.EditSelfResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	userId := bingameutils.GetUserIdFromCtx(ctx)
	us.logger.Info("EditSelf called", "userId", userId)

	err := us.handler.EditSelf(ctx, r.Msg.Nickname, r.Msg.Emoji)
	if err != nil {
		us.logger.Error("Failed to edit user details", "error", err, "userId", userId)
		return nil, err
	}

	us.logger.Info("User details edited successfully", "userId", userId)
	return connect.NewResponse(&usersv1.EditSelfResponse{}), nil
}
